from ubuntu:latest

MAINTAINER tondo

ENV INSTALLATION_DIR /usr/local

RUN DEBIAN_FRONTEND=noninteractive apt-get -yqq update \
 && DEBIAN_FRONTEND=noninteractive apt-get -yqq dist-upgrade -y \   
 && DEBIAN_FRONTEND=noninteractive apt-get -yqq install build-essential valgrind \
    cmake gcc-multilib autoconf ccache quilt \libncurses5-dev zlib1g-dev \
    sudo vim curl wget unzip python3 python3-pip gawk flex gettext locales \
    subversion git-core libboost-regex-dev libcurl4-openssl-dev libmosquitto-dev libssl-dev \
    mosquitto-dev libjson-c-dev libmodbus-dev libmodbus5 mc tcpdump \
 && useradd -m eclipse -s /bin/bash -u 1001 -G dialout \
 && echo 'eclipse ALL=NOPASSWD: ALL' > /etc/sudoers.d/eclipse \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN locale-gen en_US.UTF-8
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'
USER eclipse 
